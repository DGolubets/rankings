lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .settings(
    name := "services",
    organization := "ru.dgolubets",
    version := "0.1-SNAPSHOT",
    scalaVersion := "2.12.6",
    libraryDependencies ++= Seq(
      ws,
      "org.scalamock" %% "scalamock" % "4.1.0" % Test,
      "org.scalatest" %% "scalatest" % "3.0.4" % Test,
      "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test
    )
  )