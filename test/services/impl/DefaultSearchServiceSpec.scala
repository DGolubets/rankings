package services.impl

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink
import model.{Ranking, SearchCompleted, SearchResult}
import org.scalamock.scalatest.AsyncMockFactory
import org.scalatest.{AsyncWordSpec, BeforeAndAfterAll, Matchers}
import services.RankingService

import scala.concurrent.{Future, Promise, TimeoutException}
import scala.concurrent.duration._

class DefaultSearchServiceSpec extends AsyncWordSpec with AsyncMockFactory with Matchers with BeforeAndAfterAll {

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()

  override def afterAll = {
    system.terminate()
  }

  "DefaultSearchService" should {

    "query services simultaneously" in {
      val mars = mock[RankingService]
      val jupiter = mock[RankingService]
      val rankingServices = Map(
        "mars" -> mars,
        "jupiter" -> jupiter
      )

      (mars.query _).expects().returns(Promise[List[Ranking]]().future)
      (jupiter.query _).expects().returns(Promise[List[Ranking]]().future)

      val service = new DefaultSearchService(rankingServices, Nil, shouldRemoveAboveMedian = false)
      service
        .search()
        .completionTimeout(1 second)
        .runWith(Sink.head)
        .failed
        .map { case _: TimeoutException => succeed }
    }

    "sort results and remove duplicates" in {
      val mars = mock[RankingService]
      val jupiter = mock[RankingService]
      val rankingServices = Map(
        "mars" -> mars,
        "jupiter" -> jupiter
      )

      val marsRankings = List(
        Ranking("mars-1", "mars", 12, 100.20),
        Ranking("mars-2", "mars", 10, 12.50),
        Ranking("mars-3", "mars", 15, 40.00),
      )

      val jupiterRankings = List(
        Ranking("jupiter-1", "jupiter", 20, 100.20),
        Ranking("jupiter-2", "jupiter", 10, 12.50),
        Ranking("jupiter-3", "jupiter", 15, 20.00),
      )

      val expectedRankings = List(
        Ranking("jupiter-2", "jupiter", 10, 12.50),
        Ranking("mars-1", "mars", 12, 100.20),
        Ranking("jupiter-3", "jupiter", 15, 20.00),
        Ranking("mars-3", "mars", 15, 40.00),
        Ranking("jupiter-1", "jupiter", 20, 100.20),
      )

      (mars.query _).expects().returns(Future.successful(marsRankings))
      (jupiter.query _).expects().returns(Future.successful(jupiterRankings))

      val service = new DefaultSearchService(rankingServices, Nil, shouldRemoveAboveMedian = false)
      service
        .search()
        .collect {
          case SearchCompleted(rankings, _) => rankings
        }
        .runWith(Sink.head)
        .map(_ shouldBe expectedRankings)
    }

    "remove prices above median" in {
      val mars = mock[RankingService]
      val jupiter = mock[RankingService]
      val rankingServices = Map(
        "mars" -> mars,
        "jupiter" -> jupiter
      )

      val marsRankings = List(
        Ranking("mars-1", "mars", 12, 100.20),
        Ranking("mars-2", "mars", 10, 12.50),
        Ranking("mars-3", "mars", 15, 40.00),
      )

      val jupiterRankings = List(
        Ranking("jupiter-1", "jupiter", 20, 100.20),
        Ranking("jupiter-2", "jupiter", 10, 12.50),
        Ranking("jupiter-3", "jupiter", 15, 20.00),
      )

      // median price is 30

      val expectedRankings = List(
        Ranking("jupiter-2", "jupiter", 10, 12.50),
        Ranking("jupiter-3", "jupiter", 15, 20.00)
      )

      (mars.query _).expects().returns(Future.successful(marsRankings))
      (jupiter.query _).expects().returns(Future.successful(jupiterRankings))

      val service = new DefaultSearchService(rankingServices, Nil, shouldRemoveAboveMedian = true)
      service
        .search()
        .collect {
          case SearchCompleted(rankings, _) => rankings
        }
        .runWith(Sink.head)
        .map(_ shouldBe expectedRankings)
    }

    "return intermediate results as soon as possible" in {
      val mars = mock[RankingService]
      val jupiter = mock[RankingService]
      val rankingServices = Map(
        "mars" -> mars,
        "jupiter" -> jupiter
      )

      val marsRankings = List(
        Ranking("mars-1", "mars", 12, 100.20),
        Ranking("mars-2", "mars", 10, 12.50),
        Ranking("mars-3", "mars", 15, 40.00),
      )

      val expectedRankings = List(
        Ranking("mars-2", "mars", 10, 12.50),
        Ranking("mars-1", "mars", 12, 100.20),
        Ranking("mars-3", "mars", 15, 40.00),
      )

      (mars.query _).expects().returns(Future.successful(marsRankings))
      (jupiter.query _).expects().returns(Promise[List[Ranking]]().future)

      val service = new DefaultSearchService(rankingServices, List(100 millis), shouldRemoveAboveMedian = false)
      service
        .search()
        .completionTimeout(1 second)
        .runWith(Sink.head)
        .map { case SearchResult(rankings) => rankings shouldBe expectedRankings }
    }
  }
}
