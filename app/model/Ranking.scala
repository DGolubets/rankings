package model

case class Ranking(id: String,
                   service: String,
                   rank: Int,
                   price: BigDecimal)