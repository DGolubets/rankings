package model

import scala.concurrent.duration.FiniteDuration

trait SearchNotification

case class SearchResult(rankings: List[Ranking]) extends SearchNotification

case class SearchCompleted(rankings: List[Ranking],
                           responseTimes: Map[String, FiniteDuration]) extends SearchNotification