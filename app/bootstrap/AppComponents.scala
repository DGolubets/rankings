package bootstrap

import controllers.{ApiController, HomeController}
import play.api.ApplicationLoader.Context
import play.api.BuiltInComponentsFromContext
import play.api.libs.ws.ahc.AhcWSComponents
import play.filters.HttpFiltersComponents
import router.Routes
import services.impl.{DefaultSearchService, HttpRankingService}

import scala.concurrent.duration._
import scala.language.postfixOps

class AppComponents(context: Context)
  extends BuiltInComponentsFromContext(context)
    with HttpFiltersComponents
    with AhcWSComponents {

  implicit lazy val searchService = {

    val rankingServices = List("mars", "saturnus", "jupiter").map { name =>
      name -> new HttpRankingService(
        wsClient,
        s"https://6p9tp6qjdf.execute-api.eu-west-1.amazonaws.com/latest/res?service=$name"
      )
    }.toMap

    new DefaultSearchService(rankingServices, List(1500 millis, 2000 millis), shouldRemoveAboveMedian = true)
  }

  lazy val homeController = new HomeController(controllerComponents)

  lazy val apiController = new ApiController(controllerComponents)

  lazy val router = new Routes(httpErrorHandler, homeController, apiController)
}