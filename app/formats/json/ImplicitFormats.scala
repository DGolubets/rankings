package formats.json

import model.{Ranking, SearchCompleted, SearchNotification, SearchResult}
import play.api.libs.json._

import scala.concurrent.duration.FiniteDuration

object ImplicitFormats {
  implicit lazy val rankingFormats: Format[Ranking] = Json.format[Ranking]
  implicit lazy val searchResultWrites: Writes[SearchResult] = Json.writes[SearchResult]
  implicit lazy val searchCompletedWrites: Writes[SearchCompleted] = Json.writes[SearchCompleted]
  implicit lazy val searchNotificationWrites: Writes[SearchNotification] = new Writes[SearchNotification] {
    override def writes(o: SearchNotification): JsValue = {
      o match {
        case r: SearchResult => searchResultWrites.writes(r)
        case r: SearchCompleted => searchCompletedWrites.writes(r)
      }
    }
  }
  implicit lazy val durationWrites: Writes[FiniteDuration] = new Writes[FiniteDuration] {
    override def writes(o: FiniteDuration): JsValue = {
      JsNumber(o.toMillis)
    }
  }
}
