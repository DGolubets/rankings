package controllers

import akka.stream.scaladsl._
import play.api.mvc._
import play.api.libs.json._
import services.SearchService
import formats.json.ImplicitFormats._

class ApiController(cc: ControllerComponents)(implicit searchService: SearchService) extends AbstractController(cc) {
  def search = WebSocket.accept[String, String] { _ =>
    val in = Sink.ignore
    val out = searchService.search().map(r => Json.toJson(r).toString)
    Flow.fromSinkAndSource(in, out)
  }
}
