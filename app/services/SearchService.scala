package services

import akka.NotUsed
import akka.stream.scaladsl.Source
import model.SearchNotification

trait SearchService {
  def search(): Source[SearchNotification, NotUsed]
}
