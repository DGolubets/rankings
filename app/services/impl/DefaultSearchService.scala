package services.impl

import java.util.concurrent.TimeUnit

import akka.NotUsed
import akka.stream.scaladsl.Source
import model.{Ranking, SearchCompleted, SearchNotification, SearchResult}
import services.{RankingService, SearchService}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration.{Duration, FiniteDuration}

class DefaultSearchService(rankingServices: Map[String, RankingService],
                           flushIntervals: List[FiniteDuration],
                           shouldRemoveAboveMedian: Boolean) extends SearchService {

  import DefaultSearchService._

  override def search(): Source[SearchNotification, NotUsed] = {
    Source(rankingServices)
      .mapAsyncUnordered(parallelism = rankingServices.size) {
        case (name, s) => query(s, name)
      }
      .merge(flushTimer)
      .statefulMapConcat { () =>

        var rankings = List.empty[Ranking]
        var responseTimes = Map.empty[String, FiniteDuration]

        result => {
          result match {
            case r: QueryResult =>
              responseTimes += r.serviceName -> r.time
              rankings = aggregate(rankings, r.rankings)
              if (responseTimes.size == rankingServices.size) {
                val filteredRankings =
                  if(shouldRemoveAboveMedian)
                    removeAboveMedian(rankings)
                  else rankings

                SearchCompleted(
                  filteredRankings,
                  responseTimes) :: Nil
              }
              else Nil
            case Flush =>
              SearchResult(rankings) :: Nil
          }
        }
      }
      .filter {
        case r: SearchResult => r.rankings.nonEmpty
        case _: SearchCompleted => true
      }
      .takeWhile({
        case _: SearchCompleted => false
        case _ => true
      }, inclusive = true)
  }

  private def flushTimer: Source[Flush.type, NotUsed] = {
    val incrementalIntervals = flushIntervals.headOption.toList :::
      flushIntervals.sorted.sliding(2).collect {
        case t0 :: t1 :: _ => t1 - t0
        case t0 :: _ => t0
      }.toList

    Source(incrementalIntervals)
      .flatMapConcat { interval =>
        // one time tick
        Source
          .tick(interval, FiniteDuration(1, TimeUnit.DAYS), Flush)
          .take(1)
      }
  }

  private def removeAboveMedian(rankings: List[Ranking]): List[Ranking] = {
    val prices = rankings.map(_.price).distinct.sorted.toVector
    median(prices)
      .map { m =>
        rankings.filter(_.price <= m)
      }
      .getOrElse(rankings)
  }

  private def median(prices: Vector[BigDecimal]): Option[BigDecimal] = {
    if (prices.size > 1) {
      val median = if (prices.size % 2 == 0) {
        val i = prices.size / 2
        (prices(i - 1) + prices(i)) / 2
      } else {
        prices(prices.size / 2)
      }
      Some(median)
    }
    else None
  }

  private def aggregate(previous: List[Ranking], rankings: List[Ranking]): List[Ranking] = {
    (previous ++ rankings)
      .groupBy(r => (r.rank, r.price))
      .map { case (_, r) => r.minBy(_.id) } // make it deterministic for sake of easier testing in this demo app
      .toList
      .sortBy(r => (r.rank, r.price))
  }

  private def query(service: RankingService, serviceName: String): Future[QueryResult] = {
    measured(service.query()).map { r =>
      QueryResult(serviceName, r.value, r.time)
    }
  }

  private def measured[T](f: => Future[T]): Future[Measured[T]] = {
    val t0 = System.nanoTime
    f.map { result =>
      val duration = Duration(System.nanoTime - t0, TimeUnit.NANOSECONDS)
      Measured(result, duration)
    }
  }
}

object DefaultSearchService {

  private case class Measured[T](value: T, time: FiniteDuration)

  private sealed trait StreamResult

  private case class QueryResult(serviceName: String, rankings: List[Ranking], time: FiniteDuration) extends StreamResult

  private object Flush extends StreamResult

}