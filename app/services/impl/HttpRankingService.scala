package services.impl

import model.Ranking
import play.api.libs.json.{Json, Reads}
import play.api.libs.ws.WSClient
import services.RankingService

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class HttpRankingService(ws: WSClient,
                         uri: String) extends RankingService {

  private implicit val rankingReads: Reads[Ranking] = Json.reads[Ranking]

  override def query(): Future[List[Ranking]] = {
    ws
      .url(uri)
      .get()
      .map(_.json.as[List[Ranking]])
  }
}
