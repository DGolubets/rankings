package services

import model.Ranking

import scala.concurrent.Future

trait RankingService {
  def query(): Future[List[Ranking]]
}
